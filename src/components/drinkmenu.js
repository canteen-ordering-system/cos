export default function DrinksMenu() {
    return (  
        
            <div className="menu">
                <h3>Drinks</h3>
                <table>
                    <tr>
                        <th>Items</th>
                        <th>Price</th>
                    </tr>
                    <tr>
                        <td>Bobo Tea</td>
                        <td>$15</td>
                    </tr>
                    <tr>
                        <td>Iced Coffee</td>
                        <td>$15</td>
                    </tr>
                    <tr>
                        <td>Capaccino</td>
                        <td>$15</td>
                    </tr>
                    <tr>
                        <td>Lemon Tea</td>
                        <td>$15</td>
                    </tr>
                </table>
            </div>
        
    );
}

