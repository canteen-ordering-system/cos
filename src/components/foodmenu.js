export default function FoodMenu() {
    return (  
        <div>
            <h1>GCIT CANTEEN</h1>
            
            <div className="menu">
            <h2>Canteen Menu</h2>
                <h3>Foods</h3>
                <table>
                    <tr>
                        <th>Items</th>
                        <th>Price</th>
                    </tr>
                    <tr>
                        <td>Momo</td>
                        <td>$15</td>
                    </tr>
                    <tr>
                        <td>Pizza</td>
                        <td>$15</td>
                    </tr>
                    <tr>
                        <td>Rice</td>
                        <td>$15</td>
                    </tr>
                </table>
            </div>
        </div>
    );
}

