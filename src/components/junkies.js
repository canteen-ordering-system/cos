export default function Junkies() {
    return (  
        
            <div className="menu">
                <h3>Junkies</h3>
                <table>
                    <tr>
                        <th>Items</th>
                        <th>Price</th>
                    </tr>
                    <tr>
                        <td>French Fries</td>
                        <td>$15</td>
                    </tr>
                    <tr>
                        <td>Spring Potato</td>
                        <td>$15</td>
                    </tr>
                    <tr>
                        <td>Burger</td>
                        <td>$15</td>
                    </tr>
                    <tr>
                        <td>Chipsy</td>
                        <td>$15</td>
                    </tr>
                </table>
            </div>
        
    );
}

