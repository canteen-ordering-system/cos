import React from 'react';
// import "https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js";
// import "https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js";

function Footer() {
    const year = new Date().getFullYear();
        return (
            <div className='footer'>
                <p>Gyalpozhing College of Information Technology</p>
                <p>Gyalpozhing Iteria</p>
                <p>Copyright ⓒ {year}</p>
                <div className="social">
                    <i className="fab fa-facebook"></i>
                    <i className="fab fa-instagram"></i>
                    <i className="fab fa-pinterest"></i>
                    <i className="fab fa-google-plus"></i>
                </div>
            </div>
        );
    }

export default Footer;