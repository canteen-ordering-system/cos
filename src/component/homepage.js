import React from "react";
import Pizza from "./img/pizza.png";
import "./css/nav.css";

function HomePage() {
  return (
    <div>
      <section class="home" id="home">
        <div class="home-content">
          <div class="inner-content">
            <h3>Welcome To Iteria</h3>
            <p>
              It serves delicious foods at affordable prices based on the order.{" "}
            </p>
            <p>
              You can always place an order at any time and enjoy with your
              colleagues.
            </p>
            <p>Always there for you :</p>
          </div>
          <div class="inner-content-img">
            <img src={Pizza} alt="#"></img>
          </div>
        </div>
      </section>
    </div>
  );
}

export default HomePage;
