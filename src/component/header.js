import React from "react";
import Logo from "./img/logo-black.png";
import "./css/nav.css";

function Header() {
  return (
    <div>
      <nav class="navbar">
        <div class="logo">
          <img src={Logo} alt="#" />
        </div>
        <ul class="nav-list">
          <li>
            <a href="#about us">About Us</a>
          </li>
          <li>
            <a href="#menu">Menu</a>
          </li>
          <li>
            <a href="#order">Order</a>
          </li>
        </ul>
      </nav>
    </div>
  );
}

export default Header;
