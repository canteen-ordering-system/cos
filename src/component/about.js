import React from "react";
import VMC from "./vmc";
import "./css/about.css";

function About() {
  return (
    <div>
      <section className="Section_about">
        <div className="container_about">
          <div>
            <h3 className="main-heading_about">Our Website</h3>
            <hr className="underline" />
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
              commodo ligula eget dolor. Aenean massa. Cum sociis natoque
              penatibus et magnis dis parturient montes, nascetur ridiculus mus.
              Donec quam felis, ultricies nec, pellentesque eu, pretium quis,
              sem. Nulla consequat massa quis enim. Donec pede justo, fringilla
              vel, aliquet nec, vulputate
            </p>
            <button className="btn">Read More</button>
          </div>
        </div>
      </section>
      {/* Mission, Vision, Value */}
      <VMC />

      {/* Services */}
      <section className="Section_about back_color">
        <div className="container_about">
          <div>
            <h3 className="main-heading_about">Services</h3>
            <hr className="underline" />
          </div>
          <div className="grid-container_about">
            <div className="grid-item">
              <div className="grid__item"></div>
              <p>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
                commodo ligula eget dolor. Aenean massa. Cum sociis natoque
                penatibus et magnis dis parturient montes, nascetur ridiculus
                mus. Donec quam felis, ultricies nec, pellentesque eu, pretium
                quis, sem.
              </p>
              <button className="btn">Read More</button>
            </div>

            <div className="grid-item">
              <div className="grid__item"></div>
              <p>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
                commodo ligula eget dolor. Aenean massa. Cum sociis natoque
                penatibus et magnis dis parturient montes, nascetur ridiculus
                mus. Donec quam felis, ultricies nec, pellentesque eu, pretium
                quis, sem.
              </p>
              <button className="btn">Read More</button>
            </div>
            <div className="grid-item">
              <div className="grid__item"></div>
              <p>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
                commodo ligula eget dolor. Aenean massa. Cum sociis natoque
                penatibus et magnis dis parturient montes, nascetur ridiculus
                mus. Donec quam felis, ultricies nec, pellentesque eu, pretium
                quis, sem.
              </p>
              <button className="btn">Read More</button>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default About;
