import './App.css';
import FoodMenu from './components/foodmenu';
import DrinksMenu from './components/drinkmenu';
import Junkies from './components/junkies';
import Images from './components/img';
import About from "./component/about";
import HomePage from "./component/homepage";
import Header from "./component/header";
import Footer from "./component/footer";

function App() {
  return (
    <div className="App">
         <Header />
      <HomePage />
      <About />
      <FoodMenu/>
      <DrinksMenu/>
      <Junkies/>
      <Images/>
      
      <Footer />
    </div>
  );
}

export default App;